/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author slmfr
 */
public class Supplier {
        private int id;
        private String name;

    public Supplier() {
        this.id = -1;
        this.name = "";
    }

    public Supplier(String name) {
        this.id = -1;
        this.name = name;
    }

    public Supplier(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
    
        public static Supplier fromRS(ResultSet rs) {
       Supplier supplier = new Supplier();
        try {
            supplier.setId(rs.getInt("supplier_id"));
            supplier.setName(rs.getString("supplier_name"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return supplier;
    }

    @Override
    public String toString() {
        return "Supplier{" + "id=" + id + ", name=" + name + '}';
    }
        
}

