/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EliteCorps
 */
public class Material {
    private int id;
    private String name;
    private int min_qty;
    private int qty;
    private String unit;
    private float price_per_unit;

    public Material(int id, String name, int min_qty, int qty, String unit, float price_per_unit) {
        this.id = id;
        this.name = name;
        this.min_qty = min_qty;
        this.qty = qty;
        this.unit = unit;
        this.price_per_unit = price_per_unit;
    }

    public Material(String name, int min_qty, int qty, String unit, float price_per_unit) {
        this.id = -1;
        this.name = name;
        this.min_qty = min_qty;
        this.qty = qty;
        this.unit = unit;
        this.price_per_unit = price_per_unit;
    }

    public Material() {
        this.id = -1;
        this.name = "";
        this.min_qty = 0;
        this.qty = 0;
        this.unit = "";
        this.price_per_unit = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMin_qty() {
        return min_qty;
    }

    public void setMin_qty(int min_qty) {
        this.min_qty = min_qty;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getPrice_per_unit() {
        return price_per_unit;
    }

    public void setPrice_per_unit(float price_per_unit) {
        this.price_per_unit = price_per_unit;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", min_qty=" + min_qty + ", qty=" + qty + ", unit=" + unit + ", price_per_unit=" + price_per_unit + '}';
    }
   
    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("mat_id"));
            material.setName(rs.getString("mat_name"));
            material.setMin_qty(rs.getInt("mat_min_qty"));
            material.setQty(rs.getInt("mat_qty"));
            material.setUnit(rs.getString("mat_unit"));
            material.setPrice_per_unit(rs.getFloat("mat_price_per_unit"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }
}
