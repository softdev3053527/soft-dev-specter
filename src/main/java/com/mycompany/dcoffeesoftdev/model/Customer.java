/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Love_
 */
public class Customer {
    private int id;
    private String firstname;
    private String lastname;
    private String start_date;
    private int point;
    private String tel;
    
    public Customer(int id, String firstname, String lastname, String start_date, int point, String tel) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.start_date = start_date;
        this.point = point;
        this.tel = tel;
    }
    
    public Customer(String firstname, String lastname, String start_date, int point, String tel) {
        this.id = -1;
        this.firstname = firstname;
        this.lastname = lastname;
        this.start_date = start_date;
        this.point = point;
        this.tel = tel;
    }
    
    public Customer() {
        this.id = -1;
        this.firstname = "";
        this.lastname = "";
        this.start_date = "";
        this.point = 0;
        this.tel = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }


    
    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        try {
            customer.setId(rs.getInt("customer_id"));
            customer.setFirstname(rs.getString("customer_firstname"));
            customer.setLastname(rs.getString("customer_lastname"));
            customer.setStart_date(rs.getString("customer_start_date"));
            customer.setPoint(rs.getInt("customer_point"));
            customer.setTel(rs.getString("customer_tel"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customer;
    }
}