/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class Store {
    private int id;
    private String name;
    private String address;
    private String tel;

    public Store(int id, String name, String address, String tel) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.tel = tel;
    }

    public Store(String name, String address, String tel) {
        this.id = -1;
        this.name = name;
        this.address = address;
        this.tel = tel;
    }

    public Store() {
        this.id = -1;
        this.name = "";
        this.address = "";
        this.tel = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Store{" + "id=" + id + ", name=" + name + ", address=" + address + ", tel=" + tel + '}';
    }
    
    public static Store fromRS(ResultSet rs) {
        Store store = new Store();
        try {
            store.setId(rs.getInt("store_id"));
            store.setName(rs.getString("store_name"));
            store.setAddress(rs.getString("store_address"));
            store.setTel(rs.getString("store_tel"));
        } catch (SQLException ex) {
            Logger.getLogger(Store.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return store;
    }

}
