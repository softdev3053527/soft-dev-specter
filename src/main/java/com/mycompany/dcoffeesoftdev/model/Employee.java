/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Employee {

    private int id;
    private String name;
    private float work_hourly_wage;
    private String tel_number;
    private String address;
    private String email;
    private String position;

    public Employee(int id, String name, float work_hourly_wage, String tel_number, String address, String email, String position) {
        this.id = id;
        this.name = name;
        this.work_hourly_wage = work_hourly_wage;
        this.tel_number = tel_number;
        this.address = address;
        this.email = email;
        this.position = position;
    }

    public Employee(String name, float work_hourly_wage, String tel_number, String address, String email, String position) {
        this.id = -1;
        this.name = name;
        this.work_hourly_wage = work_hourly_wage;
        this.tel_number = tel_number;
        this.address = address;
        this.email = email;
        this.position = position;
    }

    public Employee() {
        this.id = -1;
        this.name = "";
        this.work_hourly_wage = 0;
        this.tel_number = "";
        this.address = "";
        this.email = "";
        this.position = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getWork_Hourly_Wage() {
        return work_hourly_wage;
    }

    public void setWork_Hourly_Wage(int work_hourly_wage) {
        this.work_hourly_wage = work_hourly_wage;
    }

    public String getTel_Number() {
        return tel_number;
    }

    public void setTel_Number(String tel_number) {
        this.tel_number = tel_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", work hourly wage=" + work_hourly_wage + ", tel=" + tel_number + ", address=" + address + ", email=" + email + ", position=" + position + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("emp_id"));
            employee.setName(rs.getString("emp_name"));
            employee.setWork_Hourly_Wage(rs.getInt("emp_work_hourly_wage"));
            employee.setTel_Number(rs.getString("emp_tel_number"));
            employee.setAddress(rs.getNString("emp_address"));
            employee.setEmail(rs.getNString("emp_email"));
            employee.setPosition(rs.getNString("emp_position"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
}
