/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.StoreDao;
import com.mycompany.dcoffeesoftdev.model.Store;
import java.util.List;

/**
 *
 * @author asus
 */
public class StoreService {

    public List<Store> getStore() {
        StoreDao storeDao = new StoreDao();
        return storeDao.getAll(" store_id asc");
    }

    public Store addStore(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.save(editedStore);
    }

    public Store updateStore(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.update(editedStore);
    }

    public int deleteStore(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.delete(editedStore);
    }

}
