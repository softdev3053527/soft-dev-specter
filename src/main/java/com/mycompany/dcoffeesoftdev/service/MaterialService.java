/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeesoftdev.service;

import com.mycompany.dcoffeesoftdev.dao.MaterialDao;
import com.mycompany.dcoffeesoftdev.model.Material;
import java.util.List;

/**
 *
 * @author EliteCorps
 */
public class MaterialService {
    public List<Material> getMaterials() {
        MaterialDao matDao = new MaterialDao();
        return matDao.getAll(" mat_id asc");
    } 
    
    public Material addMaterial(Material editedMaterial) {
        MaterialDao matDao = new MaterialDao();
        return matDao.save(editedMaterial);
    }
    
    public Material updateMaterial(Material editedMaterial) {
        MaterialDao matDao = new MaterialDao();
        return matDao.update(editedMaterial);
    }
    
    public int deleteMaterial(Material editedMaterial) {
        MaterialDao matDao = new MaterialDao();
        return matDao.delete(editedMaterial);
    }
}
